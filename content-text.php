<?php
	
	$body_text = array();
	if (strpos(get_locale(), 'es') !== false) {
	    //Spanish

		$body_text = array(
			'cover' => array('permiso' => 'Permiso de Publicidad'),
			'about' => array(
				'title-about' => 'Quiénes Somos',
				'text-about-1' => 'Esfera Médica es un concepto que integra tecnologías innovadoras de la medicina no invasiva probadas a nivel mundial que ayudan a hacer un diagnóstico para brindar un tratamiento específico para cada paciente.',
				'text-about-2' => 'En Esfera Médica contamos con profesionales de la salud que han sido capacitados en el extranjero para brindar atención personalizada y de calidad a nuestros pacientes bajo el precepto de <i>“la combinación del uso de tecnología y calidez humana cerca de ti”</i>.',				
			),

			'pilares' => array(
				'title' => 'Nuestra Misión',
				'p-1' => 'Integrar innovadoras tecnologías de diferentes partes del mundo para brindar ayuda en diagnósticos y tratamientos efectivos y personalizados para mejorar la salud de cada paciente.',
				'title-2' => 'Nuestra Visión',
				'p-2' => 'Ser agentes de cambio hacia el nuevo paradigma de salud (no invasiva) centrada en el paciente.',
				'title-3' => 'Nuestros Valores',
				'list-1' => '
					<li>Integridad</li>
					<li>Excelencia</li>
					<li>Innovación</li>
				',
			),

			'resonancia' => array(
				'title-1' => 'Resonancia No Lineal',
				'title-2' => 'Ventajas:',
				'p-1' => 'Es una tecnología que permite hacer un estudio detallado de todo el cuerpo, a través de un análisis muy sencillo, utilizando solamente un resonador de tejidos que se conecta a una computadora, el cual brinda a los médicos información de manera casi inmediata y los apoya a realizar un diagnóstico que les permite ofrecer al paciente un tratamiento adecuado.',
				'list-1' => '
					<li>Ayuda a definir un diagnóstico del malestar.</li>
					<li>Recomendable para la obtención de información en diferentes terapias. </li>
					<li>Rápido y cómodo.</li>
					<li>Soporte auxiliar al analizar diversas patologías, inclusive durante el tratamiento.</li>
					<li>Mayor rango de edad para el análisis.</li>
				',
			),
			'atlas' => array(
				'title-1' => 'Tecnología de apoyo para la corrección postural',
				'p-1' => 'Es una tecnología que a través de los músculos y fascias musculares de cuello ayuda a identificar las diferentes cargas o los diferentes ejes postulares incorrectos.',
				'p-2' => 'AtlasProfilax tiene como objetivo el de ayudar a corregir y alinear la postura a través de las fascias musculares que nacen en el cuello y es suficiente realizarlo sólo una vez (en la mayoría de los casos).',
				'p-3' => 'Una vez que las fascias del cuello y sus ejes (mandíbula, cuello, hombros, cadera, rodillas y tobillos) se alinean, esta tecnología no invasiva ayuda a liberar tensiones musculares.',
			),
			'electro' => array(
				'title-1' => 'Sistema Electroestimulador',
				'p-1' => 'Método de apoyo de electroestimulación neuroadaptativa para proporcionar tratamiento no invasivo terapéutico general a los sistemas fisiológicos del cuerpo por medio de las áreas de la piel humana para poder tratar diferentes patologías.',
			),
			'monalisa' => array(
				'title-1' => 'Monalisa Touch',
				'p-1' => 'Es el primer láser de apoyo ginecológico específicamente diseñado para tratar algunas de las disfunciones vulvovaginales. Este sistema emplea un innovador láser de CO2 fraccional que provoca una contracción inmediata de las paredes vaginales, favoreciendo la producción de colágeno, incrementa vascularización sanguínea y permeabilidad de las mucosas, creando un verdadero rejuvenecimiento vulvovaginal, que permite la recuperación de la funcionalidad, elasticidad y lubricación de la vagina.',
				'p-2' => 'Es un tratamiento mínimamente invasivo, en ocasiones indoloro o de molestias ligeras (puede variar de persona en persona).',
			),
			'pluri' => array(
				'title-1' => 'Tecnología Plurifuncional',
				'p-1' => 'El equilibrio de la boca es más que dientes alineados, es función y forma de todo el cuerpo. La mala alineación bucal puede provocar tensión y dolor muscular, falta de concentración, apariencia desarmonizada o envejecida, mala postura y pisada, problemas digestivos, bajo rendimiento y migraña.',
				'p-2' => 'Somos representantes certificados en México y Latinoamérica del método de apoyo francés de auto-activación Tecnología Plurifuncional, el cual ayuda a mantener la lengua en una posición correcta, restablecer la respiración nasal, abre las vías respiratorias, permitiendo en varios casos el aumento de la dimensión cervical favoreciendo la función y apariencia, podría servir como guía a los dientes que están por erupcionar, se ha utilizado para impulsa la mandíbula hacia delante liberando tensión, cuyos beneficios de esto se pueden considerar que mejora la oxigenación y circulación al respirar.',
			),

			'agustin' => array(
				'title-1' => 'Dr. Agustín Gómez',
				'p-1' => 'Cédula profesional <b class="mpro-bold">IPN 3973092</b>',
				'p-2' => 'Es egresado del Instituto Politécnico Nacional (IPN). Su interés como médico por encontrar las causas de las enfermedades lo ha llevado a explorar diferentes métodos y tratamientos.',
				'p-3' => 'En el año 2002 inició uno de sus proyectos más importantes que ha marcado su vida profesional: Esfera Médica, una clínica especializada de atención médica y enfocada en concentrar nuevas tecnologías médicas a nivel mundial que apoyan a encontrar las causas de las enfermedades. Tecnologías innovadoras que hoy en día, a su consideración, han empezado a marcar la pauta de la medicina del futuro.',
				'p-4' => 'Esfera Médica trabaja bajo el precepto de la innovación, ofreciendo a sus pacientes nuevas tecnologías complementarias a sus tratamientos médicos, que buscan mejorar su salud y calidad de vida.'
			),
			'ignacio' => array(
				'title-1' => 'Dr. Ignacio Martínez',
				'p-1' => 'Cédula profesional <b class="mpro-bold">UAM 2282844</b>',
				'p-2' => 'El Dr. Ignacio Martínez es un médico cirujano por la Universidad Autónoma Metropolitana (UAM). Apasionado de su profesión y líder en innovación al combinar diversos métodos de medicina alternativa con nuevas tecnologías científicas.',
				'p-3' => 'Sus logros profesionales están ligados a su visión de la vida: plantear y cumplir objetivos. Gracias a su intensiva preparación, carisma y talento, es un exitoso médico que ha logrado cumplir todos sus sueños, ayudando a sus pacientes a encontrar la salud, compartiendo su conocimiento e investigación, y capacitándose todos los días para ofrecer lo mejor del mundo a las personas que están cerca de él.',
				'p-4' => 'En busca de la innovación, el Dr. Ignacio Martínez ha explorado incansablemente diversos caminos de las ciencia y la tecnología médica; esto lo ha llevado a traer a México nuevas tecnologías a nivel mundial que ha puesto al alcance de todo el público de nuestro país a través de Esfera Médica, su proyecto de vida personal y profesional.',
			),

			'blog' => array(
				'link' => 'Leer más'
			),

			'faqs' => array(
				
				array(
					'title' => '¿Atienden a todo tipo de pacientes?',
					'p' => 'En Esfera Médica deseamos ayudar a todo tipo de pacientes; sin embargo, es hasta que les brindamos un diagnóstico correcto que podemos determinar si el tratamiento puede ser atendido por nosotros o algún otro especialista.',
				),
				array(
					'title' => '¿Necesitaré estar internado para recibir el tratamiento?',
					'p' => 'No, nuestros tratamientos no requieren que sea internado.',
				),
				array(
					'title' => '¿Cómo sé si el tratamiento está funcionando?',
					'p' => 'Las mejoras en su salud pueden variar de acuerdo al tipo de malestar, tiempo que lleve con él y el tipo de tratamiento que tome.',
				),
				array(
					'title' => '¿Puedo continuar con mis tratamientos médicos regulares?',
					'p' => 'Siempre es recomendable que continúe con los tratamiento médicos que sigue de forma paralela a los realizados por Esfera Médica.',
				),
				array(
					'title' => '¿Se requieren estudios clínicos?',
					'p' => 'Si es necesario, los tratamientos pueden requerir de información adicional como estudios clínicos, radiografías u otros para entender y tratar su malestar.',
				),
				array(
					'title' => '¿Dónde debo hacerme el tratamiento?',
					'p' => 'Los tratamientos se realizan en nuestras instalaciones.',
				),
				array(
					'title' => '¿Cuánto tiempo durará cada sesión del tratamiento?',
					'p' => 'Dependerá del tipo de malestar y el tipo de tratamiento que tome.',
				),
				array(
					'title' => '¿Debería acompañarme un familiar a las sesiones del tratamiento?',
					'p' => 'Debido a la contingencia actual por la que estamos pasando evitemos aglomeraciones, por lo que le pedimos que venga sin acompañantes, sólo en caso de ser necesario venir con un acompañante.',
				),
				array(
					'title' => '¿Hay algún efecto secundario en particular que deba comunicarle de inmediato?',
					'p' => 'En caso de molestias derivadas del tratamiento háganoslo saber de inmediato para determinar las causas.',
				),
				array(
					'title' => '¿Debo decirle qué medicamentos y/o suplementos estoy tomando actualmente?',
					'p' => 'Sí, cualquier información adicional a su evaluación es de gran ayuda para su tratamiento.',
				),
			),


			'contact' => array(
				'title' => 'Sucursales',

				'header-1' => 'Esfera Médica<br>Ciudad de México',
				'header-2' => 'Esfera Médica WTC',
				'header-3' => 'Esfera Médica<br><span style="white-space: nowrap;">Ciudad Juárez</span>',
				'header-4' => 'Esfera Médica<br>Tijuana',

				'locatio-1' => '
				<li>
					Bartolache 1111,<br/>
					Int 1, Col del Valle,<br/>
					Benito Juárez C.P 03100<br/>
					Tel. 55 5335 0030<br/>
				</li>
				',
				'locatio-2' => '
				<li>
					Montecito N. 38,<br/>
					Piso 9, oficina 17<br/> 
					Col. Nápoles<br/>
					CP. 03810, CDMX,<br/>
					Ciudad de México<br/>
					Tel. 8662 0732
				</li>
				',
				'locatio-3' => '
				<li>					
					Calle 20 de Noviembre 4957,<br/>
					Col. El Colegio, C.P 32340<br/>
					Tel. 656 611 4636<br/>
				</li>
				',
				'locatio-4' => '
				<li>
					Centro Médico Premier<br/>
					Antonio Caso 2055,<br/>
					Zona Urbana Río, C.P 22010<br/>					
					Tel. 664 634 1640
				</li>
				',
			),
		);

	}else{
		//English

		$body_text = array(
			'cover' => array('permiso' => 'Advertising Permit'),
			'about' => array(
				'title-about' => 'About us',
				'text-about-1' => 'ESFERA MEDICA is a concept that integrates innovative technologies of non-invasive medicine proven worldwide that help to make a diagnosis to provide a specific treatment for each patient.',
				'text-about-2' => 'In ESFERA MEDICA we have health professionals who have been trained abroad to provide personalized and quality care to our patients under the precept of "the combination of the use of technology and human warmth near you".',				
			),

			'pilares' => array(
				'title' => 'Our mission',
				'p-1' => 'We integrate innovative technologies from around the world to provide an effective diagnostic help and personalized treatments to improve the health of each patient. ',
				'title-2' => 'Our vision',
				'p-2' => 'Being agents of change towards the new paradigm of health (non-invasive) focused in the patient.',
				'title-3' => 'Our values',
				'list-1' => '
					<li>Integrity</li>
					<li>Excellence</li>
					<li>Innovation</li>
				',
			),

			'resonancia' => array(
				'title-1' => 'Nonlinear Resonance',
				'title-2' => 'Advantage:',
				'p-1' => 'It is a technology that allows a detailed study of the whole body, by a very simple analysis, using only a tissue resonator that connects to a computer, which provides doctors with information almost immediately and supports them to perform a diagnosis that allows them to offer the patient adequate treatment.',
				'list-1' => '
					<li>It helps define a diagnosis of discomfort. </li>
					<li>Recommended for obtaining information on different therapies. </li>
					<li>Fast and comfortable. </li>
					<li>Auxiliary support when analyzing various pathologies, including during treatment. </li>
					<li>Greater age range for the analysis.</li>
				',
			),
			'atlas' => array(
				'title-1' => 'Assistive Technology for Postural Correction',
				'p-1' => 'It is a neuromuscular massage technique that through the muscles and muscular fascia of the neck helps to identify the different loads or the different incorrect postulate axes. ',
				'p-2' => 'AtlasProfilax aims to help correct and align the posture through the muscle fascias that are born in the neck and this can be achieved in only one session (in most of the cases).',
				'p-3' => 'Once the fascia of the neck and its axes (jaw, neck, shoulders, hip, knees and ankles) are aligned, this non-invasive technology helps release muscle tension.',
			),
			'electro' => array(
				'title-1' => 'Electrostimulator System',
				'p-1' => 'Neuroadaptive electrostimulation support method to provide general therapeutic to provide general therapeutic non-invasive treatment to the physiological systems of the body via the areas of the human skin to be able to treat different pathologies.',
			),
			'monalisa' => array(
				'title-1' => 'Monalisa Touch®',
				'p-1' => 'It is the first gynecological support laser specifically designed to treat some of the vulvovaginal dysfunctions. This system uses an innovative CO2 laser that stimulate tissue regeneration and re-establish the proper functionality of urogenital involved structures .The treatment acts gently by stimulating collagen production, improving the function, increasing blood vascularization and mucosal permeability, creating a true “vulvovaginal rejuvenation”, which allows the recovery of functionality, elasticity and lubrication of the vagina.',
				'p-2' => 'It is a minimally invasive treatment, sometimes painless or slight discomfort (results may vary from person to person).',
			),
			'pluri' => array(
				'title-1' => 'Multifunctional Technology',
				'p-1' => 'The balance of the mouth is more than aligned teeth, it is function and shape of the whole body. Bad oral alignment can cause tension and muscle pain, lack of concentration, disharmonized or aged appearance, poor posture and tread, digestive problems, low performance and migraine.',
				'p-2' => 'We are certified representatives in Mexico and Latin America of the French support method of self-activation Multifunctional Technology, which helps to keep the tongue in a correct position, restore nasal breathing, open the airways, allowing in several cases to increase the dimension cervical favoring the function and appearance, it could serve as a guide to the teeth that are about to erupt, it has been used to propel the jaw forward releasing tension, the benefits of which can be considered that it improves oxygenation and circulation when breathing.',
			),

			'agustin' => array(
				'title-1' => 'M.D. Agustín Gómez ',
				'p-1' => 'Professional ID <b class="mpro-bold">IPN 3973092</b>',
				'p-2' => 'He graduated from the National Polytechnic Institute (IPN). His interest as a doctor to find the causes of diseases have led him to explore different methods and treatments. ',
				'p-3' => 'In 2002 he started one of his most important projects that has marked his professional life: Esfera Medica, a specialized clinic of medical attention and focused on concentrating new worldwide medical technologies that support finding the causes of diseases. Innovative technologies that today, in his opinion, have begun to set the tone of the medicine of the future',
				'p-4' => 'Esfera Medica works under the precept of innovation, offering its new patients complementary technologies to their medical treatments, which seek to improve their health and quality of living.'
			),
			'ignacio' => array(
				'title-1' => 'M.D. Ignacio Martínez',
				'p-1' => 'Professional ID  <b class="mpro-bold">UAM 2282844</b>',
				'p-2' => 'Ignacio Martínez is a medical surgeon from the Autonomous Metropolitan University (UAM). Passionate about his profession and innovation leader by combining different methods of alternative medicine with new scientific technologies. ',
				'p-3' => 'His professional achievements are linked to his vision of life: set and meet objectives. Thanks to his intensive preparation, charisma and talent, he is a successful doctor who has achieved all their dreams, helping their patients find health, sharing their knowledge and research, and training every day to offer the best in the world to people they are close to him. In search of innovation, M.D.',
				'p-4' => 'Ignacio Martínez has tirelessly explored various paths of medical science and technology; this has led him to bring new ones to Mexico technologies worldwide that it has made available to all the public in his country through Esfera Medica, his personal and professional life project. ',
			),

			'blog' => array(
				'link' => 'Read more'
			),

			'faqs' => array(
				
				array(
					'title' => 'Do you attend all types of patients?',
					'p' => 'In Esfera Medica we want to help all types of patients; however, it is until we give a correct diagnosis that we can determine if the treatment can be attended by us or some other specialist. ',
				),
				array(
					'title' => ' Will I need to be admitted to receive treatment? ',
					'p' => 'No, our treatments do not require admission.',
				),
				array(
					'title' => 'How do I know if the treatment is working?',
					'p' => ' Improvements in your health may vary according to the type of discomfort, time you have been with the problem and the type of treatment you´re taking. ',
				),
				array(
					'title' => 'Can I continue with my regular medical treatments? ',
					'p' => 'It is always recommended that you continue with the medical treatment parallel to those made by Esfera Medica.',
				),
				array(
					'title' => 'Are clinical studies required?',
					'p' => 'If necessary, treatments may require additional information such as studies clinicians, x-rays or others to understand and treat your discomfort. ',
				),
				array(
					'title' => 'Where should I get the treatment? ',
					'p' => 'The treatments are carried out in our facilities.',
				),
				array(
					'title' => 'How long will each treatment session last? ',
					'p' => 'It will depend on the type of discomfort and the type of treatment you take. ',
				),
				array(
					'title' => 'Should a family member accompany me to treatment sessions?',
					'p' => 'Due to the current contingency that we are going through, let&#39;s avoid crowds, so we ask you to come unaccompanied, only if it is necessary to come with a companion.',
				),
				array(
					'title' => 'Are there any side effects that I should notify you immediately?',
					'p' => ' In case of discomfort arising from the treatment let us know immediately to determine the causes. ',
				),
				array(
					'title' => 'Should I tell you what medications and / or supplements I am currently taking?',
					'p' => 'Yes, any additional information to your evaluation is of great help for your treatment',
				),
			),


			'contact' => array(
				'title' => 'Locations',

				'header-1' => 'Esfera Medica<br>Mexico City',
				'header-2' => 'Esfera Medica WTC',
				'header-3' => 'Esfera Medica<br><span style="white-space: nowrap;">Juarez</span>',
				'header-4' => 'Esfera Medica<br>Tijuana',

				'locatio-1' => '
				<li>
					Bartolache 1111,<br/>
					Int 1, Col del Valle,<br/>
					Benito Juárez C.P 03100<br/>
					Tel. (52+) 55 53 35 00 30<br/>
				</li>
				',
				'locatio-2' => '
				<li>
					Montecito 38<br/>
					9th floor, Suite 17 <br/> 
					Nápoles, Zip Code 03810, <br/>
					CDMX, Mexico City<br/>
					Tel. (+52) 55 8662 0732<br/>			
				</li>
				',
				'locatio-3' => '
				<li>
					Calle 20 de Noviembre 4957,<br/>
					Col. El Colegio, C.P 32340<br/>
					Tel. (52+) 656 611 4636<br/>
				</li>
				',
				'locatio-4' => '
				<li>
					Centro Médico Premier<br/>
					Antonio Caso 2055,<br/>
					Zona Urbana Río, C.P 22010<br/>					
					Tel. (52+) 664 634 1640
				</li>
				',
			),
		);
	}

