<?php
/**
 * Esfera Medica functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Esfera_Medica
 */


function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );




	



if ( ! function_exists( 'esfera_medica_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */

	function wpse_get_template_part($slug, $name = null, $data = array()) {
	    // here we're copying more of what get_template_part is doing.
	    $templates = [];
	    $name = (string) $name;

	    if ('' !== $name) {
	        $templates[] = "{$slug}-{$name}.php";
	    }

	    $templates[] = "{$slug}.php";

	    $template = locate_template($templates, false);

	    if (!$template) {
	        return;
	    }

	    if ($data) {
	        extract($data);
	    }

	    include($template);
	}

	function esfera_medica_setup() {



		
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Esfera Medica, use a find and replace
		 * to change 'esfera-medica' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'esfera-medica', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'esfera-medica' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'esfera_medica_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'esfera_medica_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function esfera_medica_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'esfera_medica_content_width', 640 );
}
add_action( 'after_setup_theme', 'esfera_medica_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function esfera_medica_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'esfera-medica' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'esfera-medica' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'esfera_medica_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function esfera_medica_scripts() {


	wp_enqueue_style( 'esfera-medica-style', get_stylesheet_uri() );
	wp_enqueue_style( 'esfera-medica-style-em', get_stylesheet_uri() , null, false, 1.0);

	wp_enqueue_script( 'esfera-medica-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'esfera-medica-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'esfera_medica_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


require get_template_directory() . '/content-text.php';
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}




function wpb_custom_new_menu() {
  register_nav_menu('my-em-menu',__( 'Esferica medica Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}


// Register Custom Post Type
function custom_post_video() {

	$labels = array(
		'name'                  => _x( 'Videos', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Video Blog', 'text_domain' ),
		'name_admin_bar'        => __( 'Video Blog', 'text_domain' ),
		'archives'              => __( 'Link Video', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'Videos', 'text_domain' ),
		'add_new_item'          => __( 'Agregar Nuevo Video Blog', 'text_domain' ),
		'add_new'               => __( 'Nuevo Video Blog', 'text_domain' ),
		'new_item'              => __( 'Nuevo Video Blog', 'text_domain' ),
		'edit_item'             => __( 'Editar Video Blog', 'text_domain' ),
		'update_item'           => __( 'Actualizar Video Blog', 'text_domain' ),
		'view_item'             => __( 'Ver Video Blog', 'text_domain' ),
		'view_items'            => __( 'Ver lista de Video Blog', 'text_domain' ),
		'search_items'          => __( 'Buscar Video Blog', 'text_domain' ),
		'not_found'             => __( 'Video Blog no encontrado', 'text_domain' ),
		'not_found_in_trash'    => __( 'Video Blog no encontrado en papelera', 'text_domain' ),
		'featured_image'        => __( 'Miniatura de Video', 'text_domain' ),
		'set_featured_image'    => __( 'Agregar Miniatura', 'text_domain' ),
		'remove_featured_image' => __( 'Usar miniatura', 'text_domain' ),
		'insert_into_item'      => __( 'Insertar video Blog', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Video', 'text_domain' ),
		'description'           => __( 'Lista de vidos para el Video Blog', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'thumbnail', 'custom-fields', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'video',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'custom_post_video', $args );

}
add_action( 'init', 'custom_post_video', 0 );


if ( $menu_items = wp_get_nav_menu_items( 'menu' ) ) {
   
   // Loop over menu items
   foreach ( $menu_items as $menu_item ) {

      // Compare menu object with current page menu object
      $current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
      
      // Print menu item
      echo '<li class="' . $current . '"><a href="' . $menu_item->url . '">' . $menu_item->title . 'XXXXX</a></li>';
   }
}




