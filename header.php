<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Esfera_Medica
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- <title>Esfera Medica</title> -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/style.css?v=0.3.23">
		
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri()?>/js/px.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
	<?php wp_head(); ?>

	<style type="text/css">
		.db-spinner {
		    width: 75px;
		    height: 75px;
		    border-radius: 50%;
		    background-color: transparent;
		    border: 2px solid #222;
		    border-top: 2px solid orange;
		    border-bottom: 2px solid orange;
		    -webkit-animation: 1s spin linear infinite;
		    animation: 1s spin linear infinite;
		}

		@-webkit-keyframes spin {
		    from {
		        -webkit-transform: rotate(0deg);
		        transform: rotate(0deg);
		    }
		    to {
		        -webkit-transform: rotate(360deg);
		        transform: rotate(360deg);
		    }
		}
		          
		@keyframes spin {
		    from {
		        -webkit-transform: rotate(0deg);
		        transform: rotate(0deg);
		    }
		    to {
		        -webkit-transform: rotate(360deg);
		        transform: rotate(360deg);
		    }
		}    

	</style>
	<style type="text/css"> html{margin-top: 0px !important; } </style>
</head>
<body onload="loadScreen()" <?php body_class(); ?>>


<div class="init-load">
	<div class="db-spinner center-loader"></div>
</div>