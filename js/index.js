$(document).ready(function(){
	$(".dropdown-item-").on('click', function(event) {
		if (this.hash !== "") {

		  var itemsMenu = $('.dropdown-item');
		  itemsMenu.each(function(index, el) {
			$(el).removeClass('active-x')
		  });

		  $(event.target).addClass('active-x');
		  event.preventDefault();
		  var hash = this.hash;
		  $('html, body').animate({
		    scrollTop: $(hash).offset().top
		  }, 600, function(){
		    window.location.hash = hash;
		  });
		} 
	});

	(function($) {
	    $.fn.hasScrollBar = function() {
	        return this.get(0).scrollHeight > this.height();
	    }

	})(jQuery);

	function showSecction(elem){
		var element = document.querySelector(elem);
		var position = element.getBoundingClientRect();					
		if(position.top >= 0 && position.bottom <= window.innerHeight) {						
			$('#title-menu').text($(elem).attr('title-mb'));
		}
		if(position.top < window.innerHeight && position.bottom >= 0) {						
			$('#title-menu').text($(elem).attr('title-mb'));
		}
	}

	window.addEventListener('scroll', function() {});


	function checkScreenSize(size, minBG, maxBG) {
	  var x = window.matchMedia(size)
	  if (x.matches) { 
	    minBG();
	  } else {				
	  	maxBG();
	  }
	  x.addListener(checkScreenSize)
	}
	

	checkScreenSize('(max-width: 700px)',function(){
			
		$('#dr-Agustin-gomez').parallax({
			position:'650px 0px',
			imageSrc:'<?php echo get_template_directory_uri()?>/img/9mobile.jpg'
		});
		$('#dr-ignacio-martinez').parallax({
			position:'650px 0px',
			imageSrc:'<?php echo get_template_directory_uri()?>/img/10mobile.jpg'
		});
		$('#monalisa-touch').parallax({
			position:'660px 0px',
			imageSrc:'<?php echo get_template_directory_uri()?>/img/7mobile.jpg'
		});

	},function(){
		
		$('#dr-Agustin-gomez').parallax({
			imageSrc:'<?php echo get_template_directory_uri()?>/img/9.png'
		});
		$('#dr-ignacio-martinez').parallax({
			imageSrc:'<?php echo get_template_directory_uri()?>/img/10.png'
		});
		$('#monalisa-touch').parallax({
			imageSrc:'<?php echo get_template_directory_uri()?>/img/7.png'
		});

	}); 

	
	$('.dropdown-item').on('click', function(){
	    $('.navbar-collapse').collapse('hide');
	});

	$('.menu-alone').on('click', function(){
	    $('.navbar-collapse').collapse('hide');
	});


	var itemsVideoBlog = $('.item-video-blog-em');
	var itemsVideoBlogFirst = $(itemsVideoBlog[0]);
	$('#box-video').attr('src',itemsVideoBlogFirst.attr('youtube-url') );

	itemsVideoBlog.click( function(event) {					
		
		var currentElement = $(this);

		itemsVideoBlog.each(function(index, el) {
			$(el).removeClass('video-active')
		});

		currentElement.addClass('video-active');
		var url = currentElement.attr('youtube-url');

		$('#box-video').attr('src',url);
	});

	var containerInfo = $('.container-info');
	containerInfo.toggle('fast');
	$('.icon-add-circle').on('click', function(event) {			
		containerInfo.toggle('slow');
		$('.icon-add-circle').hide('slow');
	});

	currentMenuItem = $('a[href="<?php echo get_permalink()?>"]');
	currentMenuItem.each(function(index, el) {
		$(el).addClass('active-x');
	});
});

function showReload(){
	$('.init-load').fadeIn('500');
}

function hideReload(){
	$('.init-load').fadeOut('500', function() {
			
	});
}


$('[href]').on('click', function(event) {				
	console.log(event);
	if($(event.target).attr('data-toggle') != 'dropdown' && !( $(event.target).hasClass('dropdown-item') ) ){
		showReload();					
	}		
});

function loadScreen(){
	hideReload();
}


setTimeout(function(){ hideReload(); }, 6000);
