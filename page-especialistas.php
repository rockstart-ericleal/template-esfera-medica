<?php 

/* 

Template Name: Especialistas 
*/ 

get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'agustin', $body_text ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'ignacio', $body_text ); ?>


<?php
get_footer();