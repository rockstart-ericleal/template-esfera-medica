<?php 

/* 

Template Name: FAQs 
*/ 

get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'faqs', $body_text ); ?>

<?php
get_footer();