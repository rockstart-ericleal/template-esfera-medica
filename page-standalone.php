<?php
/* 

Template Name: Stand Alone 
*/ 
get_header();
?>



<?php get_template_part( 'template-parts/content', 'menu' ); ?>

<?php get_template_part( 'template-parts/content', 'cover' ); ?>
<?php get_template_part( 'template-parts/content', 'about' ); ?>
<?php get_template_part( 'template-parts/content', 'pilares' ); ?>
<?php get_template_part( 'template-parts/content', 'metraton' ); ?>
<?php get_template_part( 'template-parts/content', 'biomecanica' ); ?>
<?php get_template_part( 'template-parts/content', 'electro' ); ?>
<?php get_template_part( 'template-parts/content', 'monalisa' ); ?>
<?php get_template_part( 'template-parts/content', 'plurifuncionales' ); ?>
<?php get_template_part( 'template-parts/content', 'agustin' ); ?>
<?php get_template_part( 'template-parts/content', 'ignacio' ); ?>
<?php get_template_part( 'template-parts/content', 'blog' ); ?>
<?php get_template_part( 'template-parts/content', 'faqs' ); ?>
<?php get_template_part( 'template-parts/content', 'contact' ); ?>








<?php
get_footer();