<?php 

/* 

Template Name: Tecnologías General 
*/ 

get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>

<?php wpse_get_template_part( 'template-parts/content', 'metraton', $body_text ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'biomecanica', $body_text ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'electro', $body_text ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'monalisa', $body_text ); ?>
<?php wpse_get_template_part( 'template-parts/content', 'plurifuncionales', $body_text ); ?>


<?php
get_footer();