<?php 

/* 

Template Name: Video Blog 
*/ 

get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>
<?php get_template_part( 'template-parts/content', 'video' ); ?>

<?php
get_footer();