<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Esfera_Medica
 */

get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>
<!-- Content Blog -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-fluid no-gutters"  data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg_blog.jpg">
		<div class="row h-full">
			
			
			<div class="col-6 d-none d-lg-block">
				<div class="row h-100">
					<div class="col-12 align-self-center text-right">
						<?php echo get_the_post_thumbnail( $post_id, null, array( 'class' => 'img-cover-post' ) ); ?>
						<!-- <img class="img-cover-post " src="https://via.placeholder.com/780x512"> -->
					</div>
				</div>
			</div>
			

			<div class="col-12 col-lg-6 mt-5mt-5 overflow-y-scroll">
				<div class="container">

					<div class="space-white"></div>

					<div class="col-12 d-sm-block d-lg-none text-center">
						<?php echo get_the_post_thumbnail( $post_id, null, array( 'class' => 'img-cover-post p-4' ) ); ?>
						<!-- <img class="img-cover-post p-4" src="https://via.placeholder.com/780x512"> -->
					</div>

					<?php the_title( '<h1 class="text-center mb-4 rem-2-3 mpro-bold mark-menu-about"  title-mb="BLOG" >', '</h1>' ); ?>				
					<div class="container">
						<div class="row  justify-content-center content-post">

							<div class="col-12 col-md-10">
								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>									
									<?php the_content(); ?>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>	
						</div>
					</div>

					<div class="space-white"></div>
					
				</div>
			</div>


		</div>
	</div>
</article>


<?php

get_footer();
