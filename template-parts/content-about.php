<!-- About -->
<section class="position-relative" id="about-us" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-2.jpg">
	<div class="container">	
		<figure class="position-absolute icon-logo-em-right ">
			<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
		</figure>

		<div class="container-fluid no-gutters" >
			<div class="row h-full pt-5">
				<div class="col-6 d-none d-lg-block"></div>
				<div class="col-12 col-lg-6 my-auto mt-5">
					<div class="container">
						<h1 class="text-center mb-4 rem-2-8 mark-menu-about mpro-bold"  title-mb="NOSOTROS" ><?php echo $about['title-about']?></h1>
						<div class="container">
							<div class="row  justify-content-center">
								<div class="col-12 col-md-10 mpro-rg">
									<p class="text-justify"><?php echo $about['text-about-1']?></p>
									<p class="text-justify"><?php echo $about['text-about-2']?></p>
								</div>	
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>				
