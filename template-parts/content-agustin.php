<!-- Dr. Agustín Gómez  data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/9.png"-->
<section class="position-relative" id="dr-Agustin-gomez" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/9mobile.jpg">

	<figure class="position-absolute icon-logo-em-left d-none d-md-block">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

	<div class="container" >			

		<div class="row h-full">
			<div class="col-12 col-lg-6 my-auto">
				<div class="container fra mark-menu-especial-1"  title-mb="ESPECIALISTAS">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-10 mpro-rg">
							<h1 class="text-center rem-2-8 mpro-bold"><?php echo $agustin['title-1']?></h1>
							<h3 class="text-center rem-1-2 mb-4"><?php echo $agustin['p-1']?></h3>
							<p class="text-justify"><?php echo $agustin['p-2']?></p>
							<p class="text-justify"><?php echo $agustin['p-3']?></p>
							<p class="text-justify"><?php echo $agustin['p-4']?></p>
							<div class="col-12 d-block d-md-none">
								<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
							</div>
						</div>	
					</div>

				</div>
			</div>

			

			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>