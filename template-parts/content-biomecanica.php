<!-- Correccion Biomecanica AtlaxProfilax -->
<section class="position-relative" id="biomecanica-atlaxprofilax" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-5.jpg">
	
	<figure class="position-absolute icon-logo-em-left">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

	<div class="container-fluid no-gutters " >
		<div class="row h-full">
			<div class="col-12 col-lg-6 my-auto">
				<div class="container fra mt-5">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-10 mpro-rg px-0 px-md-5">
							<h1 class="text-center rem-2-3 mark-menu-tecno-2 mpro-bold"  title-mb="TECNOLOGÍAS"><?php echo $atlas['title-1']?></h1>
							<p class="text-justify"><?php echo $atlas['p-1']?></p>
							<p class="text-justify"><?php echo $atlas['p-2']?></p>
							<p class="text-justify"><?php echo $atlas['p-3']?></p>

						</div>
					</div>

				</div>
			</div>
			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>