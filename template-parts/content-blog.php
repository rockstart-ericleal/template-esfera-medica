<!-- BLOG! -->
<section id="blog">
<div class="container-fluid no-gutters  mpro-rg position-relative" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg_blog.jpg">

<figure class="position-absolute icon-logo-em-left d-none d-md-block">
	<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
</figure>

<div class="container">
	<div class="row h-full pb-2 mt-4 mark-menu-blog-1"  title-mb="BLOG" >
		<div class="col-12 align-self-center my-5">
			<div class="row box-post-container justify-content-center mb-5">

			<?php
				global $post;
			    $myposts = get_posts( array(
			        'posts_per_page' => 12,
			    ) );
			    if ( $myposts ) {
				    foreach ( $myposts as $post ) :  setup_postdata( $post ); ?>
				        <div class="col-12 col-md-6 col-lg-4 ">
							<div class="row m-2">
								<div class="col-12 box-container-img-post-em position-relative">
									<a href="<?php the_permalink(); ?>">
										<div class="layer-hover-post position-absolute"></div>
										<?php echo get_the_post_thumbnail( $post_id, 'full', array( 'class' => 'img-post-em' ) ); ?>										
									</a>
								</div>
								<div class="col-12">
									<h1 class="rem-1-2 text-center mt-2 mpro-bold"><?php the_title(); ?></h1>
								</div>
								<div class="col-12 text-right pr-0 rem-1-2">
									<a href="<?php the_permalink(); ?>"><?php echo $blog['link']?></a>
								</div>
							</div>
						</div>				       
				    <?php
				    endforeach;
				    wp_reset_postdata();
				}else{
					?>
						¡No hay artículos!
					<?php
				}
			?>


			</div>
		</div>
	</div>
	</div>
</div>
</section>