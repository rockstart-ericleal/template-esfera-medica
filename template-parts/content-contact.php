<!-- Footer -->
<section class="position-relative" id="contact" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-12.png">
	
	<div class="position-absolute permiso">
			<h3><b><?php echo $cover['permiso']?></b> 2033002T1A0009</h3>
		</div>

		<!-- <div class="position-absolute box-icon-container icon-contact">
			<figure class="mark-menu">
				<img class="log-icon-em" src="<?php echo get_template_directory_uri()?>/img/logo-em.png">
			</figure>
		</div> -->

	<footer>
		<div class="container-fluid no-gutters section-contact text-center" >

			<div class="row h-full">
				<div class="col-12 align-self-start mt-5">
					<div class="container">
						<h1 class="mt-5 mb-5 mark-menu-contact-1 mpro-bold"  title-mb="SUCURSALES"><?php echo $contact['title']?></h1>
						<div class="row my-4 mpro-rg t-contact">
							
							<div class="px-4 px-lg-2 col-6 col-md-6 col-lg-4">
								<ul>
									<li class="rem-1-4 mpro-bold"><b><?php echo $contact['header-1']?></b></li>
									<?php echo $contact['locatio-1']?>
								</ul>
							</div>

	<!-- 						<div class="px-4 px-lg-2 col-6 col-md-6 col-lg-3">
								<ul>
									<li class="rem-1-4 mpro-bold"><b><?php echo $contact['header-2']?></b></li>
									<?php echo $contact['locatio-2']?>
								</ul>
							</div> -->
						
							<div class="px-4 px-lg-2 col-6 col-md-6 col-lg-4">
								<ul>
									<li class="rem-1-4 mpro-bold"><b><?php echo $contact['header-3']?></b></li>
									<?php echo $contact['locatio-3']?>
								</ul>
							</div>
							<div class="px-4 px-lg-2 col-6 col-md-6 col-lg-4">
								<ul>
									<li class="rem-1-4 mpro-bold"><b><?php echo $contact['header-4']?></b></li>
									<?php echo $contact['locatio-4']?>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</footer>

</section>