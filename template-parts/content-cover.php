<!-- Cover -->
<section id="about" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-1.jpg">
	<div class="container-fluid no-gutters position-relative " >

		<div class="position-absolute box-icon-container">
			<figure class="mark-menu" title-mb="NOSOTROS">
				<img class="log-icon-em" src="<?php echo get_template_directory_uri()?>/img/logo-em.png">
			</figure>
		</div>
		<div class="box-menu-container position-absolute d-none d-lg-block ">
			<ul>
				<!-- <li>
					<a href="https://www.facebook.com/EsferaMedicaMX/">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-fb.svg">
					</a>
				</li>
				<li>
					<a href="https://twitter.com/Esfera_Medica">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-tw.svg">
					</a>
				</li>
				<li>
					<a href="https://www.instagram.com/esferamedica/">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-ig.svg">
					</a>
				</li>
				<li>
					<a href="https://www.linkedin.com/in/esfera-medica-325242197/">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-in.svg">
					</a>
				</li>
				<li>
					<a href="https://open.spotify.com/user/w06grwje20vu7prusq22lxwjv?si=SDXbbXTQSpKBZv6tsct00w">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-spfy.svg">
					</a>
				</li>
				<li>
					<a href="https://www.youtube.com/channel/UCS7ndwmDXUUkjpabWoZTMmA">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-ytb.svg">
					</a>
				</li> -->
				<!-- <li>
					<a href="">
						<img src="<?php echo get_template_directory_uri()?>/img/icons/icon-ws.svg">
					</a>
				</li> -->
			</ul>
		</div>
		<div class="row h-full"></div>
	</div>
</section>