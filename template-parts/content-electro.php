<!-- Sistema Electro -->
<section class="position-relative" id="sistema-neuroadaptivo" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-6.png">
	
	<figure class="position-absolute icon-logo-em-left">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

	<div class="container-fluid no-gutters" >
		<div class="row h-full">
			<div class="col-12 col-lg-6 my-auto">
				<div class="container fra mark-menu-tecno-5"  title-mb="TECNOLOGÍAS">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-8 px-0 mpro-rg">
							<h1 class="text-center mpro-bold rem-2-3"><?php echo $electro['title-1']?></h1>
							<p class="text-justify px-lg-5"><?php echo $electro['p-1']?></p>					
						</div>	
					</div>
				</div>
			</div>
			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>