<!-- FAQs -->
<section class="position-relative faqs-image h-full" id="faqs" style="background-image: url('<?php echo get_template_directory_uri()?>/img/bg-11.jpg');">
	<div class="container-fluid  no-gutters" >

	<figure class="position-absolute icon-logo-em-right d-none d-md-block">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

		<div class="row h-full ">
			<div class="col-12 col-lg-8 my-auto mark-menu-faqs-1"  title-mb="FAQs">
				<div class="container mt-5 pt-2 mpro-normal">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-9 mb-5">

							<ul class="list-faqs mt-4">

								<?php

									foreach ($faqs as $key => $value) {
										
										?>
											<li><?php echo $value['title']?></li>
											<p class="text-left rem--7"><?php echo $value['p']?></p>
										<?php																	
									}
								?>
							

							</ul>
							<div class="col-12 d-block d-md-none mb-2">
								<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="col-4 d-none d-lg-block"></div>
		</div>
	</div>
</section>