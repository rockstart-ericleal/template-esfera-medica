<!-- Dr. Ignacio Martínez -->
<section class="position-relative" id="dr-ignacio-martinez" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/10mobile.jpg">

	<figure class="position-absolute icon-logo-em-left d-none d-md-block">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

	<div class="container" >

		

		<div class="row h-full">
			<div class="col-12 col-lg-6 my-auto">
				<div class="container fra mark-menu-especial-2"  title-mb="ESPECIALISTAS">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-10 mpro-rg">
							<h1 class="text-center rem-2-5 mpro-bold"><?php echo $ignacio['title-1'] ?></h1>
							<h3 class="text-center rem-1-2 mb-4"><?php echo $ignacio['p-1'] ?></h3>
							<p class="text-justify"><?php echo $ignacio['p-2'] ?></p>
							<p class="text-justify"><?php echo $ignacio['p-3'] ?></p>
							<p class="text-justify"><?php echo $ignacio['p-4'] ?></p>
							<div class="col-12 d-block d-md-none">
								<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
							</div>
						</div>
					</div>
					
				</div>
			</div>

			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>