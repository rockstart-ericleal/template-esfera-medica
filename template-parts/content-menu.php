
<?php


$dom_com = ".com";
$dom_mx = ".mx";
 
$section = basename(get_permalink());
$title_section = '';
// Test if string contains the word 
if( strpos($section, $dom_com) !== false || strpos($section, $dom_mx) !== false ){
    $title_section = 'INICIO';
}


if($section === 'tecnologias'){
	$title_section = 'TECNOLOGÍAS';
}


switch ($section) {
    case 'nosotros':
        $title_section = 'NOSOTROS';
        break;
    case 'tecnologias':
        $title_section = 'TECNOLOGÍAS';
        break;
    case 'especialistas':
        $title_section = 'ESPECIALISTAS';
        break;
    case 'blog':
        $title_section = 'BLOG';
        break;
    case 'faqs':
        $title_section = 'FAQs';
        break;
    case 'contacto':
        $title_section = 'CONTACTO';
        break;
}

//$title_section = strtoupper($title_section);

?>


<div class="content content-navbar fixed-top mt-lg-2">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand color-white" href="/">
			<span class="d-block d-lg-none" id="title-menu"><?php echo $title_section ?></span>
			
			<div class="menu-lang d-none d-lg-block">
				<a href="<?php echo pll_the_languages(array('raw'=>1))['es']['url'];?>">ESPAÑOL</a>
			 	<div class="bullet-content">
			 		<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfkAgURHw47Gbq4AAACyElEQVRo3s2ZPWhTURiG33NMXZrQpKbpViqFREmKJXTN2FFRKA4iFYroItQfcBARoUOoUhdHf6pUXf1ZS8HqrKVpAsbFokONSdqUxKFD8zokN7m0Jcm9ufcc3+1y4Tzv933355zvE7AohpBADCcQRgB+eAFUUMI2sviGDD6JvNUVO0eP8xHXWWUrVZniPONOo328yQytKM0b9DoDD/A+i5bghgq8R393cMEp5mzBDRV5hcIufoSfu4IbWuFxO/iz3HIET5I7PG819Q8dg9dUZbLjUtDDZw7ja1pkTyf4Hn5wBU+S7+lpn/wF1/Ak+apNIRyv/UElW+EnXceTVZ4zM00J4Qi+oM/ay2pLJcTFD+NCNquPF0rwgB/PD3kSOK0g/U1d3FcCBpDFgJL4a8ohInbMJZhRigcGcc2UAfZiA0GlBoAihkXFyMBV5XjgGC43M7COmHIDQEbEAAlwXAseiHKsVoILWvCokSWACW0GJgDBEH7D7q6tWxEhiYQ2PCCQkJoeQENRiYhWAxGJsG4D6r+BZgUlfFoN+AR3cVSjgV3Z/RrdSaKilV+WKOs2UNBqoCDxXauBrERWt4G0VgMZwQHktP0PqxiUIq8xB2uiIAEsaTOwVNuSvdFm4LWxLU9hVAM+LUaNg8mClvifAv/L0Uz8xWPl8c+LSiMDAP3IIqQQv4mIKAON47ko4bbS+G+J/X9hCq4o648sN7nmJtUQVtGvIPptxMWGcWHakomfuAS6jiemm/iDd5Oup3+2tT3hUqPa0GLbnjk9fOca/m3bZjUA8AifuIJ/2VG7vl6IZJvxnFVVOWtxdsQzNmdlh6nESTvvyzA/OoJf5pANfN3Eaf7qCr7JKdtju7qFPt5l3hb8D+/QmbM3vbzOlCX4GmfY6wjcZGOMD7jKvZbgPX7lHE91vqrl+jCIBKI4iTD6TeP7rcb4vmhtvX8QZymhn6283gAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMC0wMi0wNVQxNzozMToxNCswMDowMKzIc4kAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjAtMDItMDVUMTc6MzE6MTQrMDA6MDDdlcs1AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==">
			 	</div>
			 	<a href="<?php echo pll_the_languages(array('raw'=>1))['en']['url'];?>">ENGLISH</a>
			</div>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>


		<?php #var_dump(pll_the_languages(array('raw'=>1))['es']['url'])?>
		
		<?php

			wp_nav_menu( array(
			    'theme_location'  => 'my-em-menu',
			    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
			    'container'       => 'div',
			    'container_class' => 'navbar-collapse x-menu collapse',
			    'container_id'    => 'navbarNavDropdown',
			    'menu_class'      => 'navbar-nav ml-auto tw menu-index-first mm',
			    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			    'walker'          => new WP_Bootstrap_Navwalker(),
			) );

		?>

	</nav>
</div>