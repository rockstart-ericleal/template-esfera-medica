<!-- Metraton - Introducer -->
<section id="metraton-introducer" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-4.png">
	<div class="container-fluid no-gutters position-relative" >

	<figure class="position-absolute icon-logo-em-right">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

		<div class="row h-full">
			<div class="col-12 col-lg-6 my-auto">
				<div class="container">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-10 px-0 px-md-5">
							<h1 class="text-center rem-2-3  mpro-bold mark-menu-tecno-1"  title-mb="TECNOLOGÍAS"><?php echo $resonancia['title-1']?></h1>
							<p class="text-justify mpro-rg"><?php echo $resonancia['p-1']?></p>
							<div class="icon-add-circle button-show-list"></div>
							<div class="container-info">
								<h1 class="text-left rem-1 mpro-bold"><?php echo $resonancia['title-2']?></h1>
								<ul class="mpro-rg">
									<?php echo $resonancia['list-1']?>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>