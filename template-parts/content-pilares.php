<!-- Pilares -->
<section class="position-relative" id="pilares" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/bg-3.jpg">
	
	<figure class="position-absolute icon-logo-em-left">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

	<div class="container-fluid no-gutters" >
		<div class="row h-full">
			<div class="col-12 col-lg-6 my-auto">
				<div class="container">
					
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-10 px-0 px-md-5">
							<h1 class="text-left rem-2 mpro-bold pl-4 mark-menu-about-1"  title-mb="NOSOTROS" ><?php echo $pilares['title']?></h1>
							<p class="mpro-rg text-justify"><?php echo $pilares['p-1']?></p>
							
							<h1 class="text-left rem-2 mpro-bold pl-4"><?php echo $pilares['title-2']?></h1>
							<p class="text-left mpro-rg"><?php echo $pilares['p-2']?></p>
							
							<h1 class="text-left rem-2 mpro-bold pl-4"><?php echo $pilares['title-3']?></h1>
							
							<ul class="mpro-rg pres-list">
								<?php echo $pilares['list-1']?>
							</ul>
							
						</div>	
					</div>

				</div>
			</div>
			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>