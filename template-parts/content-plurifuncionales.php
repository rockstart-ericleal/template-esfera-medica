<!-- Actividades Plurifuncionales -->
<section class="position-relative" id="actividades-plurifuncionales" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri()?>/img/8.png">
	
	<figure class="position-absolute icon-logo-em-left">
		<img class="icon-logo-about" src="<?php echo get_template_directory_uri()?>/img/logo-em-2.png">
	</figure>

	<div class="container-fluid no-gutters" >
		<div class="row h-full pt-5">
			<div class="col-12 h-100 col-lg-6 my-auto">
				<div class="container fra">
					
					<div class="row justify-content-center">
						<div class="col-12 col-md-10 mpro-rg">
							<h1 class="text-center rem-2 mark-menu-tecno-3 mpro-bold"  title-mb="TECNOLOGÍAS"><?php echo $pluri['title-1'] ?></h1>
							<div class="px-0 px-md-5">
								<p class="text-justify"><?php echo $pluri['p-1'] ?></p>
								<p class="text-justify"><?php echo $pluri['p-2'] ?></p>
							
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<div class="col-6 d-none d-lg-block"></div>
		</div>
	</div>
</section>