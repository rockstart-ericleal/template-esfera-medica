<!-- Video Blog! -->
<section id="video-blog">
	<div class="container  h-full video-blog-em position-relative" >

		<!-- <figure class="position-absolute icon-logo-em-right d-none d-md-block">
			<img class="icon-logo-about mt-2 mb-2 min-icon-logo" src="./img/logo-em.png">
		</figure> -->

		<div class="row h-full mark-menu-videoblog-1"  title-mb="VIDEOBLOG" >
			
			<!-- video-active  -->
			<div class="pt-5 overflow-y-scroll col-12 col-md-4  order-2 order-md-1 align-self-center h-full bg-grey">						
				
				<ul>

					<?php

					$args = array (
	                  'post_type'              => array( 'custom_post_video' ),
	                  'post_status'            => array( 'publish' ),
	                  'nopaging'               => true,
	                  'order'                  => 'DESC'	                  
	                );


					$services = new WP_Query( $args );

					while ( $services->have_posts() ) {
                    	$services->the_post();                
                    ?>
                    	<li class="m-4"><img class="video-active item-video-blog-em" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0];?>" youtube-url="<?php echo get_post_field('LINK')?>"></li>
                    <?php
                	}
					?>
							
				</ul>
			
			</div>
			<!-- video-active  -->


			<?php wp_reset_postdata(); ?>

			<div class="box-video-content col-12 col-md-8 order-1 order-md-2 align-self-center text-center">
				
				<div class="">
					<iframe id="box-video" class="video-youtube-em" width="560" height="415" src="https://www.youtube.com/embed/VE_pN7A1fWY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>							
				</div>
				
			</div>
		</div>
	</div>
</section>